<?php

namespace Drupal\basket;

use Drupal\Core\Archiver\Zip;
use Drupal\Core\Url;

/**
 * {@inheritdoc}
 */
class Libraries {

  const CDN = [
    'codemirror' => [
      'directory' => 'codemirror',
      'version' => '5.65.12',
      'download' => 'https://github.com/components/codemirror/archive/refs/tags/5.65.12.zip',
      'config' => [
        'css' => [
          'theme' => [
            '/libraries/codemirror/lib/codemirror.css' => [],
            '/libraries/codemirror/theme/bespin.css' => [],
          ],
        ],
        'js' => [
          '/libraries/codemirror/lib/codemirror.js' => [],
          '/libraries/codemirror/addon/fold/xml-fold.js' => [],
          '/libraries/codemirror/addon/edit/matchtags.js' => [],
          '/libraries/codemirror/addon/mode/overlay.js' => [],
          '/libraries/codemirror/mode/xml/xml.js' => [],
          '/libraries/codemirror/mode/css/css.js' => [],
          '/libraries/codemirror/mode/htmlmixed/htmlmixed.js' => [],
          '/libraries/codemirror/mode/twig/twig.js' => [],
          '/libraries/codemirror/mode/clike/clike.js' => [],
          '/libraries/codemirror/mode/php/php.js' => [],
        ],
      ],
    ],
    'jquery.inputmask' => [
      'directory' => 'jquery.inputmask',
      'version' => '5.0.8',
      'download' => 'https://github.com/RobinHerbots/jquery.inputmask/archive/refs/tags/5.0.8.zip',
      'config' => [
        'js' => [
          '/libraries/jquery.inputmask/dist/jquery.inputmask.min.js' => ['minified' => TRUE],
          'misc/jquery.inputmask/basket.inputmask.js' => ['minified' => TRUE],
        ],
      ],
    ],
    'tooltipster' => [
      'directory' => 'tooltipster',
      'version' => '4.2.8',
      'download' => 'https://github.com/calebjacob/tooltipster/archive/refs/tags/4.2.8.zip',
      'config' => [
        'css' => [
          'theme' => [
            '/libraries/tooltipster/dist/css/tooltipster.bundle.min.css' => ['minified' => TRUE],
            '/libraries/tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-shadow.min.css' => ['minified' => TRUE],
          ],
        ],
        'js' => [
          '/libraries/tooltipster/dist/js/tooltipster.bundle.min.js' => ['minified' => TRUE],
        ],
      ],
    ],
    'multiple.select' => [
      'directory' => 'multiple-select',
      'version' => '1.2.1',
      'download' => 'https://github.com/wenzhixin/multiple-select/archive/refs/tags/1.2.1.zip',
      'config' => [
        'css' => [
          'theme' => [
            '/libraries/multiple-select/multiple-select.css' => ['minified' => FALSE],
          ],
        ],
        'js' => [
          '/libraries/multiple-select/multiple-select.js' => ['minified' => FALSE],
        ],
      ],
    ],
    'colorpicker' => [
      'directory' => 'tinyColorPicker',
      'version' => '1.1.1',
      'download' => 'https://github.com/PitPik/tinyColorPicker/archive/refs/tags/1.1.1.zip',
      'config' => [
        'js' => [
          '/libraries/tinyColorPicker/jqColorPicker.min.js' => ['minified' => FALSE],
        ],
      ],
    ],
  ];

  /**
   * Implements hook_library_info_alter().
   */
  public function alter(&$libraries): void {
    foreach ($this::CDN as $key => $cdn) {
      if (!empty($libraries[$key])) {
        $location = DRUPAL_ROOT . '/libraries/' . $cdn['directory'];
        if (is_dir($location)) {
          $libraries[$key] = array_merge($libraries[$key], $cdn['config']);
        }
      }
    }
  }

  /**
   * Page content.
   */
  public function pageInfo() {
    $rows = [];
    foreach ($this::CDN as $key => $cdn) {
      $location = DRUPAL_ROOT . '/libraries/' . $cdn['directory'];

      $rows[$key] = [
        $key,
        [
          'data' => [
            '#type' => 'html_tag',
            '#tag' => is_dir($location) ? 'span' : 's',
            '#value' => '/libraries/' . $cdn['directory'],
          ],
        ],
      ];
      if (!is_dir($location)) {
        $rows[$key]['cdn'] = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => 'CDN <a href="javascript:void(0);" class="button--link target" onclick="basket_admin_ajax_link(this, \'{{ url }}\')" data-post="{{ post }}">download {{ v }}</a>',
            '#context' => [
              'url' => Url::fromRoute('basket.admin.pages', ['page_type' => 'api-external_libraries'])->toString(),
              'post' => json_encode([
                'name' => $key,
              ]),
              'v' => $cdn['version'],
            ],
          ],
        ];
      }
      else {
        $rows[$key][] = 'Local files';
      }
    }
    return [
      'table' => [
        '#theme' => 'table',
        '#rows' => $rows,
      ],
      'composer' => [
        '#type' => 'details',
        '#title' => 'How to use composer to install libraries for the Basket module',
        '#open' => TRUE,
        [
          '#theme' => 'item_list',
          '#list_type' => 'ol',
          '#items' => [
            [
              '#markup' => 'The merging is accomplished by the aid of the Composer Merge Plugin plugin available on GitHub, so from the project directory, open a terminal and run: <code>composer require wikimedia/composer-merge-plugin</code>',
            ],
            [
              '#type' => 'inline_template',
              '#template' => 'Edit the "composer.json" file of your website and under the "extra": { section add:
              <code><pre>"extra": {
      ...
      "merge-plugin": {
          "include": [
              "{{ path }}/composer.libraries.json"
          ]
      }
  }</pre></code>',
              '#context' => [
                'path' => \Drupal::getContainer()->get('extension.list.module')->getPath('basket'),
              ],
            ],
            [
              '#markup' => '<code>composer update --with-dependencies</code>',
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * Library download functionality.
   */
  public function download($library_name) {
    $cdn = $this::CDN[$library_name];

    if (empty($cdn['download'])) {
      return FALSE;
    }

    $dType = NULL;
    if (preg_match('/\.zip$/', $cdn['download'])) {
      $dType = 'zip';
    }

    if ($dType == 'zip') {
      $this->downloadFile($cdn['download'], DRUPAL_ROOT . '/libraries',
        $cdn['directory']);
    }
  }

  /**
   * Archive download functionality.
   */
  protected function downloadFile($url, $library, $name) {

    $file_system = \Drupal::getContainer()->get('file_system');

    $destination_tmp = $file_system->tempnam('temporary://', 'download_file');
    $destination_tmp = $file_system->realpath($destination_tmp);

    \Drupal::httpClient()->get($url, ['sink' => $destination_tmp]);

    if (!file_exists($destination_tmp)) {
      \Drupal::messenger()->addError(t("The URL !url could not be downloaded.", ['!url' => $url]));
      return FALSE;
    }

    $zip = new Zip($destination_tmp);
    $baseDir = trim($zip->getArchive()->getNameIndex(0), '/');
    if (!empty($baseDir)) {
      $zip->extract($library);

      // Renames the directory.
      if (is_dir($library . '/' . $baseDir)) {
        rename($library . '/' . $baseDir, $library . '/' . $name);
      }
    }
    else {
      $zip->extract($library . '/' . $name);
    }

    if (file_exists($destination_tmp)) {
      unlink($destination_tmp);
    }
  }

}
