<?php

namespace Drupal\basket\Plugins\Extra;

/**
 * Provides an interface for all Basket Extra plugins.
 */
interface BasketExtraSettingsInterface {

  /**
   * Gets extra field settings form.
   */
  public function getSettingsForm();

  /**
   * Gets extra field settings summary.
   */
  public function getSettingsSummary($settings);

}
