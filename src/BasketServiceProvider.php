<?php

namespace Drupal\basket;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Alter base service provider implementation.
 */
class BasketServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('module_handler');
    $definition->setClass(ModuleHandler::class);
  }

}
