<?php

namespace Drupal\basket;

use Drupal\Core\Extension\ModuleHandler as CoreModuleHandler;

/**
 * {@inheritdoc}
 */
class ModuleHandler extends CoreModuleHandler {

  /**
   * {@inheritdoc}
   */
  public function alter($type, &$data, &$context1 = NULL, &$context2 = NULL) {

    // We exclude specific keys from alters,
    // which optimizes the operation of sites.
    $notAlters = $GLOBALS['config']['basket_module_handler_not_alters'] ?? [];
    $notAlters[] = 'form_basket-button-params-';

    if (is_array($type)) {
      foreach ($type as $key => $hook) {
        foreach ($notAlters as $notAlter) {
          if (str_contains($hook, $notAlter)) {
            unset($type[$key]);
          }
        }
      }
    }
    parent::alter($type, $data, $context1, $context2);
  }

}
