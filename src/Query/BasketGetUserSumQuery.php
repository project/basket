<?php

namespace Drupal\basket\Query;

/**
 * Calculating the total amount of user purchases.
 *
 * @deprecated in basket:2.0.0 and is removed from basket:3.0.0.
 * Use \Drupal::getContainer()->get('BasketQuery').
 * @see https://www.drupal.org/project/basket/issues/1
 */
class BasketGetUserSumQuery {

  /**
   * GetQuery.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')->getUserSumQuery().
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function getQuery() {
    return \Drupal::getContainer()->get('BasketQuery')->getUserSumQuery();
  }

  /**
   * ViewsJoin.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')->userSumViewsJoin($view).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function viewsJoin(&$view) {
    \Drupal::getContainer()->get('BasketQuery')->userSumViewsJoin($view);
  }

  /**
   * ClickSort.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')
   * ->userSumViewsJoinSort($view, $order).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function clickSort(&$view, $order) {
    \Drupal::getContainer()->get('BasketQuery')->userSumViewsJoinSort($view, $order);
  }

}
