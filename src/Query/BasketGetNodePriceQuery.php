<?php

namespace Drupal\basket\Query;

/**
 * Determination of prices for goods according to the settings.
 *
 * @deprecated in basket:2.0.0 and is removed from basket:3.0.0.
 * Use \Drupal::getContainer()->get('BasketQuery').
 * @see https://www.drupal.org/project/basket/issues/1
 */
class BasketGetNodePriceQuery {

  /**
   * Request prices.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')
   * ->getPriceQuery($keyPriceField, $entityId).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function getQuery($keyPriceField = 'MIN', $entityId = NULL) {
    return \Drupal::getContainer()->get('BasketQuery')->getPriceQuery($keyPriceField, $entityId);
  }

  /**
   * Is there a price conversion on the site?
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')->isPriceConvert().
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function isConvert() {
    return \Drupal::getContainer()->get('BasketQuery')->isPriceConvert();
  }

  /**
   * View price request.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')
   * ->priceViewsJoin($view, $keyPriceField, $filter).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function viewsJoin(&$view, $keyPriceField = 'MIN', $filter = []) {
    \Drupal::getContainer()->get('BasketQuery')->priceViewsJoin($view, $keyPriceField, $filter);
  }

  /**
   * Sort by price.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')
   * ->priceViewsJoinSort($view, $order, $keyPriceField, $filter).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function viewsJoinSort(&$view, $order, $keyPriceField = 'MIN', $filter = []) {
    \Drupal::getContainer()->get('BasketQuery')->priceViewsJoinSort($view, $order, $keyPriceField, $filter);
  }

  /**
   * Obtaining the minimum price of node.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')
   * ->getNodePriceMin($entity, $keyPriceField, $filter).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function getNodePriceMin($entity, $keyPriceField = 'MIN', $filter = []) {
    return \Drupal::getContainer()->get('BasketQuery')->getNodePriceMin($entity, $keyPriceField, $filter);
  }

}
