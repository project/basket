<?php

namespace Drupal\basket\Query;

/**
 * Determination of product balances according to the settings.
 *
 * @deprecated in basket:2.0.0 and is removed from basket:3.0.0.
 * Use \Drupal::getContainer()->get('BasketQuery').
 * @see https://www.drupal.org/project/basket/issues/1
 */
class BasketGetNodeCountsQuery {

  /**
   * Quantity request.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')->getQtyQuery($entityId).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function getQuery($entityId = NULL) {
    return \Drupal::getContainer()->get('BasketQuery')->getQtyQuery($entityId);
  }

  /**
   * Filter by quantity.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')->qtyViewsJoin($view).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function viewsJoin(&$view) {
    \Drupal::getContainer()->get('BasketQuery')->qtyViewsJoin($view);
  }

  /**
   * Sort by quantity.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0.
   * Use \Drupal::getContainer()->get('BasketQuery')->qtyViewsJoin($view).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function viewsJoinSort(&$view, $order) {
    \Drupal::getContainer()->get('BasketQuery')->qtyViewsJoinSort($view, $order);
  }

}
