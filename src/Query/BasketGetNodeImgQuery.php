<?php

namespace Drupal\basket\Query;

/**
 * Determination of the product image according to the settings.
 *
 * @deprecated in basket:2.0.0 and is removed from basket:3.0.0.
 * Use \Drupal::getContainer()->get('BasketQuery').
 * @see https://www.drupal.org/project/basket/issues/1
 */
class BasketGetNodeImgQuery {

  /**
   * Request a picture.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')->getImgQuery().
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function getQuery($entityId = NULL) {
    return \Drupal::getContainer()->get('BasketQuery')->getImgQuery($entityId);
  }

  /**
   * Request views.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')->ImgViewsJoin($view).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function viewsJoin(&$view) {
    \Drupal::getContainer()->get('BasketQuery')->ImgViewsJoin($view);
  }

  /**
   * Getting the first image of the node.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')->getNodeImgFirst($entity).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function getNodeImgFirst($entity) {
    return \Drupal::getContainer()->get('BasketQuery')->getNodeImgFirst($entity);
  }

  /**
   * Getting a default picture.
   *
   * @deprecated in basket:2.0.0 and is removed from basket:3.0.0. Use
   * \Drupal::getContainer()->get('BasketQuery')->getDefFid($entity).
   * @see https://www.drupal.org/project/basket/issues/1
   */
  public static function getDefFid($entity) {
    return \Drupal::getContainer()->get('BasketQuery')->getDefFid($entity);
  }

}
