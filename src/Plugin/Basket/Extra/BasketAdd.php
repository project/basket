<?php

namespace Drupal\basket\Plugin\Basket\Extra;

use Drupal\basket\Plugins\Extra\BasketExtraSettingsInterface;

/**
 * Plugin extra field add button.
 *
 * @BasketExtraSettings(
 *   id = "basket_add",
 *   name = "Basket Add",
 * )
 */
class BasketAdd implements BasketExtraSettingsInterface {

  /**
   * Set basket.
   *
   * @var Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Set trans.
   *
   * @var Drupal\basket\BasketTranslate
   */
  protected $trans;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->basket = \Drupal::service('Basket');
    $this->trans = $this->basket->Translate();
  }

  /**
   * Gets extra field settings form.
   */
  public function getSettingsForm() {
    $form = [];
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => $this->trans->t('Button text'),
    ];
    $form['count'] = [
      '#type' => 'checkbox',
      '#title' => $this->trans->t('Show + / -'),
    ];
    return $form;
  }

  /**
   * Gets extra field settings summary.
   */
  public function getSettingsSummary($settings) {
    return implode('<br/>', [
      $this->trans->t('Button text')->__toString() . ': ' . ($settings['text'] ?? ''),
      $this->trans->t('Show + / -')->__toString() . ': ' . (!empty($settings['count']) ? t('yes')->__toString() : t('no')->__toString()),
    ]);
  }

}
