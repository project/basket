<?php

namespace Drupal\basket\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\views\Ajax\ViewAjaxResponse;

/**
 * {@inheritdoc}
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['onResponse', 0];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function onResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    if ($response instanceof ViewAjaxResponse) {
      $view = $response->getView();
      if ($view->id() == 'basket' && $view->current_display == 'block_2') {
        $commands = &$response->getCommands();
        foreach ($commands as $key => $command) {
          if (in_array($command['command'], ['viewsScrollTop', 'scrollTop'])) {
            unset($commands[$key]);
          }
          if ($command['command'] == 'insert'
            && $command['method']
            && !empty($command['data'])
            && str_contains($command['data'],
              'data-basketid="')) {
            $commands[$key]['selector'] = '[data-basketid="view_wrap-' . $view->id() . '-' . $view->current_display . '"]';
          }
        }
      }
    }
  }

}
