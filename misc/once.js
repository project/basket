(function ($) {
  if (!$.fn.once) {
    $.fn.once = function ($name) {
      let $this = this;
      if ($this.length) {
        try {
          let elements = once($name, $this.get());
          return $(elements);
        }
        catch (e) {}
      }
      return $this;
    }
  }
})(jQuery);
