<?php

/**
 * @file
 * Describes hooks provided by the Basket module.
 */

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\views\ResultRow;

/**
 * Changing access to functionality.
 *
 * @param bool|null $access
 *   Access status.
 * @param string $permission
 *   Name of permission.
 * @param array $options
 *   Additional parameters.
 */
function hook_basket_access_alter(bool | NULL &$access, string $permission, array $options): void {

}

/**
 * Changing the quantity input parameters.
 *
 * @param array $attr
 *   Input parameters.
 * @param int $nid
 *   Node ID.
 * @param array|string|null $params
 *   Additional parameters.
 */
function hook_basket_count_input_attr_alter(array &$attr, int $nid, array | string | NULL $params): void {

}

/**
 * Changing the list of contexts used in the ecosystem.
 *
 * @param array $context
 *   List of contexts.
 */
function hook_basket_translate_context_alter(array &$context): void {

}

/**
 * Changing the parameters of adding a product to the cart.
 *
 * @param array $info
 *   Input parameters.
 */
function hook_basket_add_alter(array &$info): void {

}

/**
 * Changing the output image in views.
 *
 * @param string|null $uri
 *   Uri of the image.
 * @param \Drupal\views\ResultRow $values
 *   A class representing a view result row.
 */
function hook_basket_cart_img_alter(string | NULL &$uri, ResultRow $values): void {

}

/**
 * Changing the parameters of the add button in views.
 *
 * @param array $options
 *   Array of current settings.
 */
function hook_basket_add_field_views_defineOptions_alter(array &$options): void {

}

/**
 * Managing the add button settings form in views.
 *
 * @param array $form
 *   Form array.
 * @param array $defValues
 *   Array of current settings.
 */
function hook_basket_add_field_views_buildOptionsForm_alter(array &$form, array $defValues): void {

}

/**
 * Changing the currency when loading the page.
 *
 * @param int|null $currency
 *   The current currency identifier.
 */
function hook_basket_current_currency_alter(int | NULL&$currency): void {

}

/**
 * Changing the price request (globally for the site).
 *
 * @param \Drupal\Core\Database\Query\SelectInterface|null $query
 *   SelectInterface.
 * @param array $keyNodeTypes
 *   Material type key.
 * @param int|null $entityId
 *   Product entity ID.
 */
function hook_basketNodeGetPriceField_alter(SelectInterface | NULL &$query, array $keyNodeTypes, int | NULL $entityId): void {
  $query->addExpression('...', 'nid');
  $query->addExpression('...', 'price');
  $query->addExpression('...', 'currency');
  $query->addExpression('...', 'priceConvert');
  $query->addExpression('...', 'priceConvertOld');
}

/**
 * Modification of the final price formation request.
 *
 * @param \Drupal\Core\Database\Query\SelectInterface $query
 *   SelectInterface.
 */
function hook_priceQueryPostAlter_alter(SelectInterface &$query): void {

}

/**
 * Determination of the number of new positions.
 *
 * @param float|null $count
 *   Quantity to display.
 * @param string $type
 *   Field key.
 */
function hook_basket_get_new_count_alter(float | NULL &$count, string $type): void {

}

/**
 * Changing the payment element in the form.
 *
 * @param array $formPayment
 *   Form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Is an object that implements.
 */
function hook_basketPaymentField_alter(array &$formPayment, FormStateInterface $form_state): void {

}

/**
 * Replacing the Ajax response to changing the payment field.
 *
 * @param \Drupal\Core\Ajax\AjaxResponse $response
 *   Response object.
 */
function hook_basket_ajaxReloadPayment_alter(AjaxResponse &$response): void {

}

/**
 * Alter to make changes before creating a payment.
 *
 * @param object $order
 *   Order object data.
 * @param \Drupal\node\Entity\Node $entity
 *   Order entity.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Is an object that implements.
 */
function hook_basket_create_payment_preInit_alter(object $order, Node $entity, FormStateInterface $form_state): void {

}

/**
 * Management of access to payment points in the form.
 *
 * @param bool $access
 *   Field availability.
 * @param int $key
 *   Field key.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Is an object that implements.
 */
function hook_basket_payment_option_access_alter(bool &$access, int $key, FormStateInterface $form_state): void {

}

/**
 * Allows you to set default data before building the delivery form.
 *
 * @param array $formDelivery
 *   Form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Is an object that implements.
 */
function hook_basket_delivery_preInit_alter(array &$formDelivery, FormStateInterface $form_state): void {

}

/**
 * Management of access to delivery points in the form.
 *
 * @param bool $access
 *   Field availability.
 * @param int $key
 *   Field key.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Is an object that implements.
 */
function hook_basket_delivery_option_access_alter(bool &$access, int $key, FormStateInterface $form_state): void {

}

/**
 * Replacing the Ajax response to changing the delivery field.
 *
 * @param \Drupal\Core\Ajax\AjaxResponse $response
 *   Response object.
 */
function hook_basket_ajaxReloadDelivery_alter(AjaxResponse $response): void {

}

/**
 * Interpretation of additional product parameters.
 *
 * @param array $element
 *   An array of elements.
 * @param array|null $params
 *   Input parameters.
 * @param bool $isInline
 *   Whether to return the inline element.
 */
function hook_basket_params_definition_alter(array &$element, array | NULL $params, bool $isInline): void {

}

/**
 * Validation of parameters when adding / updating an order item.
 *
 * @param \Drupal\Core\Ajax\AjaxResponse $response
 *   Response object.
 * @param bool $isValid
 *   Add to cart, validation?
 * @param array|null $data
 *   Data from $_POST.
 */
function hook_basketValidParams_alter(AjaxResponse $response, bool &$isValid, array | NULL $data): void {

}

/**
 * Changing the list of tokens by key.
 *
 * @param array $tokens
 *   Tokens data.
 * @param string $templateType
 *   Template key.
 */
function hook_basketTemplateTokens_alter(array &$tokens, string $templateType): void {

}

/**
 * Replacement of page data by input keys.
 *
 * @param \Drupal\Core\Ajax\AjaxResponse|array $element
 *   Page content.
 * @param string|null $page_type
 *   Input parameter.
 * @param string|null $page_subtype
 *   Input parameter.
 */
function hook_basket_pages_alter(AjaxResponse | array &$element, string | NULL $page_type, string | NULL $page_subtype): void {

}

/**
 * Hook replace default order values when creating.
 *
 * @param \Drupal\node\Entity\Node $node
 *   Order Entity.
 */
function hook_basket_order_tokenDefaultValue_alter(Node $node): void {

}

/**
 * Replacement of the response after placing the order.
 *
 * @param \Drupal\Core\Ajax\AjaxResponse $response
 *   Response object.
 * @param array $form
 *   Form data.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Is an object that implements.
 */
function hook_basket_submit_ajax_response_alter(AjaxResponse &$response, array $form, FormStateInterface $form_state): void {

}

/**
 * Hook basket token value.
 *
 * @param string $value
 *   The value of the token.
 * @param string $name
 *   The name of the key.
 * @param array $params
 *   Input parameters.
 */
function hook_basketTokenValue_alter(&$value, string $name, array $params): void {

}

/**
 * Hook the order number format.
 *
 * @param int|string $orderViewId
 *   Order view ID.
 */
function hook_basket_order_get_id_alter(int | string &$orderViewId): void {
  $orderViewId = 'PREFIX-' . $orderViewId;
}

/**
 * It is used to track changes in the order.
 *
 * @param object $order
 *   Order info.
 * @param object|null $oldOrder
 *   Old Order info.
 */
function hook_basketOrderUpdate_alter(object $order, object | NULL $oldOrder): void {

}

/**
 * Control popup template input after adding to cart.
 *
 * @param array $info
 *   Additional data popup after adding the product to the cart.
 */
function hook_basket_add_popup_alter(array &$info): void {

}

/**
 * Managing the total amount of the cart after all calculations.
 *
 * @param float $getTotalSum
 *   The total amount of the order.
 * @param array|null $getItemsInBasket
 *   An array of items in the basket, or null if no items exist.
 * @param array $config
 *   Configuration options relevant to the basket.
 */
function hook_basket_getTotalSum_alter(float &$getTotalSum, array | NULL $getItemsInBasket, array $config) {

}

/**
 * Management of payment amount and currency.
 *
 * @param array $info
 *   Payment data.
 */
function hook_basket_getPayInfo_alter(array &$info): void {

}

/**
 * Manage the price determination of the item in the cart (before).
 *
 * @param float|int|string|null $price
 *   Price per cart position.
 * @param object $row
 *   Basket position.
 */
function hook_basket_getItemPrice_alter(float | int | string | NULL &$price, object $row): void {

}

/**
 * Manage the price determination of the item in the cart (after).
 *
 * @param float|int|string|null $price
 *   Price per cart position.
 * @param object $row
 *   Basket position.
 */
function hook_basket_getItemPriceAfter_alter(float | int | string | NULL &$price, object $row): void {

}

/**
 * Manage the descount determination of the item in the cart.
 *
 * @param int|null $discount
 *   Cart Item Discount (%).
 * @param object $row
 *   Basket position.
 */
function hook_basket_getItemDiscount_alter(int | NULL &$discount, object $row): void {

}

/**
 * Manage the image ID determination of the item in the cart.
 *
 * @param int|string|null $fid
 *   Image ID.
 * @param object $row
 *   Basket position.
 */
function hook_basket_getItemImg_alter(int | string | NULL &$fid, object $row): void {

}

/**
 * Allows you to override the request for receiving the user's shopping cart.
 *
 * @param \Drupal\Core\Database\Query\SelectInterface|null $query
 *   SelectInterface.
 */
function hook_basket_getItemsInBasketQuery_alter(SelectInterface | NULL &$query): void {
  $query = \Drupal::getContainer()->get('database')->select('basket', 'b');
  $query->fields('b');
  $query->condition('b.sid', \Drupal::getContainer()->get('Basket')->cart()->getSid());
  $query->innerJoin('node_field_data', 'n', 'n.nid = b.nid');
  $query->condition('n.type', 'NODE_TYPE');
}

/**
 * Management of product entity operational links.
 *
 * @param array $links
 *   An array with links.
 * @param \Drupal\node\Entity\Node $entity
 *   Product Entity.
 */
function hook_stockProductLinks_alter(array &$links, Node $entity): void {
  $links['my_link'] = [
    'text' => 'My link',
    'ico' => 'SVG data',
    'attributes' => new Attribute([
      'href' => Url::fromRoute('entity.node.canonical', ['node' => $entity->id()])->toString(),
      'target' => '_blank',
    ]),
  ];
}

/**
 * Order data management at the time of entity creation and before order.
 *
 * @param array $basketOrderFields
 *   An array with data on the order that is being processed.
 * @param array $basketItems
 *   An array with data on goods that are processed.
 * @param \Drupal\node\Entity\Node $entity
 *   Order Entity.
 */
function hook_basket_insertOrder_alter(array &$basketOrderFields, array &$basketItems, Node $entity): void {

}

/**
 * Alter which is called after placing an order.
 *
 * @param \Drupal\node\Entity\Node $entity
 *   Order Entity.
 * @param int $orderId
 *   Order ID.
 */
function hook_basket_postInsertOrder_alter(Node $entity, int $orderId): void {

}

/**
 * Hook output data on additional fields of material.
 *
 * @param array $field
 *   Field data.
 * @param int|string|null $value
 *   Settings data.
 * @param int|string $key
 *   Data key.
 */
function hook_basket_node_type_extra_fields_list_alter(array &$field, int | string | NULL $value, int | string $key): void {

}

/**
 * Hook control additional fields types of materials.
 *
 * @param array $tableForm
 *   Part of the form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   Is an object that implements.
 */
function hook_basket_node_type_extra_fields_form_alter(array &$tableForm, FormStateInterface $form_state): void {

}

/**
 * Management of order operational links.
 *
 * @param array $links
 *   An array of action.
 * @param object $order
 *   Order.
 */
function hook_basket_order_links_alter(array $links, object $order): void {

}

/**
 * We change parameters when adding a product to an order.
 *
 * @param array $data
 *   An array that undergoes changes.
 * @param \Drupal\node\Entity\Node $entity
 *   Product Entity.
 */
function hook_basket_orderEditAddData_alter(array &$data, Node $entity): void {

}

/**
 * We are changing the list of data on the settings of delivery points.
 *
 * @param array $items
 *   The array that is supplemented changes.
 * @param array $config
 *   Item settings data.
 */
function hook_delivery_settings_info_alter(array &$items, array $config): void {
  $items[] = [
    '#markup' => "Config info",
  ];
}

/**
 * We are changing the list of data on the settings of payment points.
 *
 * @param array $items
 *   The array that is supplemented changes.
 * @param array $config
 *   Item settings data.
 */
function hook_payment_settings_info_alter(array &$items, array $config): void {
  $items[] = [
    '#markup' => "Config info",
  ];
}

/**
 * We exchange data by quantity.
 *
 * @param array $loads
 *   Data array.
 */
function hook_basket_post_load_alter(array &$loads): void {

}

/**
 * We change the admin data of the shopping cart page.
 *
 * @param \Drupal\Core\Ajax\AjaxResponse|array $element
 *   Page content.
 * @param array $params
 *   Available options.
 */
function hook_basket_admin_page_alter(AjaxResponse | array &$element, array $params): void {

}

/**
 * The moment of clearing currency caches.
 */
function hook_currency_clear_cache(): void {

}

/**
 * The moment of payment confirmation by the payment system.
 *
 * @param int $nid
 *   Order Entity ID.
 */
function hook_basket_paymentFinish(int $nid): void {

}

/**
 * The moment of fixing changes in the positions of the completed order.
 *
 * @param object $orderItem
 *   Order position.
 * @param string $type
 *   Value: insert/update/delete.
 */
function hook_basket_item(object $orderItem, string $type): void {

}

/**
 * The moment of fixation of changes in the position of the basket.
 *
 * @param object $cartItem
 *   Basket position.
 * @param string $type
 *   Value: add/updateCount/delete.
 */
function hook_basket_cart(object $cartItem, string $type): void {

}

/**
 * We are changing the delivery information.
 *
 * @param array $info
 *   Delivery info.
 * @param object|null $delivery
 *   Delivery term.
 */
function hook_basket_getDeliveryInfo_alter(array &$info, ?object $delivery): void {

}
